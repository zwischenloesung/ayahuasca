from django.contrib import admin

from .models import Id, IdUrl, Thing


class IdAdmin(admin.ModelAdmin):
    list_display = ["iid", "uuid", "name", "version"]
    search_fields = ["codice_istat", "comune"]


admin.site.register(Id, IdAdmin)


class IdUrlAdmin(admin.ModelAdmin):
    pass


admin.site.register(IdUrl, IdUrlAdmin)


class ThingAdmin(admin.ModelAdmin):
    pass


admin.site.register(Thing, ThingAdmin)
