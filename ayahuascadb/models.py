from django.db import models


class Id(models.Model):
    iid = models.AutoField(primary_key=True)
    uuid = models.UUIDField(unique=True)
    name = models.CharField(max_length=80, unique=True)
    version = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return "{} {}({})".format(self.name, self.version, self.uuid)


class IdUrl(models.Model):
    iid = models.AutoField(primary_key=True)
    fid = models.ForeignKey(Id, on_delete=models.CASCADE, related_name="url")
    url = models.URLField()

    def __str__(self):
        return self.url


class Thing(models.Model):
    iid = models.OneToOneField(Id, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.iid)
